import { exec as execProcess } from 'node:child_process'
import fsp from 'node:fs/promises'
import ospath from 'node:path'
import { fileURLToPath } from 'node:url'
import Mocha from 'mocha'
import chai from 'chai'
import glob from 'glob'

const __dirname = fileURLToPath(new URL('.', import.meta.url))

async function getFiles () {
  return new Promise((resolve, reject) => {
    glob(ospath.join(__dirname, '..', 'fixtures', '**/*.input.adoc'), async function (err, files) {
      if (err) {
        reject(err)
      } else {
        resolve(files)
      }
    })
  })
}

async function exec (adapter, content) {
  return new Promise((resolve, reject) => {
    try {
      const childProcess = execProcess(adapter, (error, stdout, stderr) => {
        if (error) {
          reject(error)
          return
        }
        resolve({ stdout, stderr })
      })
      childProcess.stdin.on('error', (err) => {
        reject(err)
      })
      write(childProcess.stdin, content, () => {
        childProcess.stdin.end()
      })
    } catch (e) {
      reject(e)
    }
  })
}

function write (stream, data, cb) {
  if (!stream.write(data)) {
    stream.once('drain', cb)
  } else {
    process.nextTick(cb)
  }
}

export default async function (adapter, options) {
  const defaultOptions = {
    timeout: 60 * 1000,
  }
  if (options === undefined) {
    options = {}
  }
  options = { ...defaultOptions, ...options }
  const files = await getFiles()
  const mocha = new Mocha(options)
  const Test = Mocha.Test
  const Suite = Mocha.Suite
  const suites = {}

  for (const file of files) {
    const relative = ospath.relative(ospath.join(__dirname, '..', 'fixtures'), file)
    const suiteName = ospath.dirname(relative)
    let suite
    if (suiteName in suites) {
      suite = suites[suiteName]
    } else {
      suite = Suite.create(mocha.suite, suiteName)
      suites[suiteName] = suite
    }
    const testName = ospath.basename(file).replace(/\.input\.adoc$/, '')
    suite.addTest(
      new Test(testName, async () => {
        try {
          const input = await fsp.readFile(file, 'utf8')
          const expectedFile = ospath.join(
            __dirname,
            '..',
            'fixtures',
            relative.replace(/\.input\.adoc$/, '.output.json')
          )
          const expectedOutput = await fsp.readFile(expectedFile, 'utf8')
          const childProcess = await exec(adapter, input)
          const actualOutput = JSON.parse(childProcess.stdout)
          chai.expect(actualOutput).to.deep.equal(JSON.parse(expectedOutput))
        } catch (e) {
          chai.expect.fail(e.message)
        }
      })
    )
  }
  return new Promise((resolve) => {
    const runner = mocha.run((result) => {
      resolve({
        failures: result,
        stats: runner.stats,
      })
    })
  })
}
