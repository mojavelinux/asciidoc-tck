'use strict'
//import yargs from 'yargs'
//import { hideBin } from 'yargs/helpers'
import runner from './runner.js'

//const argv = yargs(hideBin(process.argv)).help().argv

const adapter = process.env.ASCIIDOC_TCK_ADAPTER

if (adapter) {
  ;(async () => {
    try {
      const result = await runner(adapter)
      process.exit(result.failures)
    } catch (e) {
      console.error('Something wrong happened!', e)
    }
  })()
}
