#!/usr/bin/env node

'use strict'

if (process.pkg) {
  // index.cjs is generated from index.js by Rollup
  require('../lib/index.cjs')
} else {
  import('../lib/index.js')
}
