/* eslint-env mocha */
'use strict'
import chai from 'chai'
import runner from '../lib/runner.js'

describe('runner()', () => {
  it('should fail when using an non existing adapter', async () => {
    const result = await runner('non-existing-adapter', {
      reporter: function () {
        /* noop */
      },
    })
    chai.expect(result.failures).to.eq(2)
    chai.expect(result.stats.suites).to.eq(1)
    chai.expect(result.stats.tests).to.eq(2)
    chai.expect(result.stats.failures).to.eq(2)
  })
})
